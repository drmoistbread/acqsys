﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Reader
{
    public static class ReaderFactory
    {

        // this is a simple factory for a new Data Reader
        public static IReader CreateNewReader(string reader)
        {
            switch (reader.ToLower())
            {
                case "bitalino":
                    return new BitalinoReaderAdapter();
                case "bitalino-cp":
                    return new BitalinoCPAdapter();
                default:
                    return null;

            }
        }
    }

}

