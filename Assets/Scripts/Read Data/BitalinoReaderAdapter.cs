﻿using System;
using UnityEngine;
using System.Collections;
using System.Threading;
using Filter;

namespace Reader
{
    public class BitalinoReaderAdapter : IReader
    {

        private BitalinoReader _reader;
        public string Name { get; set; }
        public int BufferSize { get; private set; }
        public int NumberOfBits { get; set; }


        public BitalinoReaderAdapter()
        {
            Name = "Bitalino Reader";

        }


    //just go through the Bitalino buffer and return the value for the desired channel.
    public double[] GetBuffer(string channelToRead, FilterManager filterManager)
        {
            double[] readData = new double[BufferSize];

            int channelNumber = int.Parse(channelToRead);

            int i = 0;

            foreach (BITalinoFrame r in _reader.getBuffer())
            {
                readData[i] = r.GetAnalogValue(channelNumber);
                readData[i] = filterManager.ApplyFilter(readData[i]);
                i++;
            }
            return readData;
        }

        public void DefineReader(string reader)
        {
            GameObject go = GameObject.Find(reader);
            _reader = (BitalinoReader)go.GetComponent(typeof(BitalinoReader));
            BufferSize = _reader.BufferSize;
            NumberOfBits = 10;
        }
    }

}