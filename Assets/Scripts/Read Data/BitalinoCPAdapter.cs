﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Devices;
using UnityEngine;
using Filter;

namespace Reader
{

    public class BitalinoCPAdapter : IReader
    {

        public string Name { get; set; }
        public int BufferSize { get; private set; }
        public int NumberOfBits { get; set; }

        private const double EMPTY = 30000000d;
        private UDPReceiveBitalino _reader;

        public BitalinoCPAdapter()
        {
            Name = "BITalino-RehabNetCP";
        }
        public double[] GetBuffer(string channelToRead,FilterManager filterManager)
        {

            double[] readData = new double[_reader.BufferSize];

            int i = 0;
            foreach (string s in _reader.getBuffer())
            {
                readData[i] = TranslateData(s, channelToRead);
                readData[i] = filterManager.ApplyFilter(readData[i]);
                i++;


            }
            return readData;
        }

        //example:
        //data:A4,0.0;A0,518.0;A1,12.0;A3,0.0;
        //channelToRead: 0;
        //return 518.0
        private double TranslateData(string data, string channelToRead)
        {

            /*string channel = "A" + channelToRead;
            Match match = Regex.Match(data, @"A\d+,\d+");

            string[] words = match.Value.Split(',');*/

            string[] separators = { "[$]", "[$$]", "[$$$]", ",", ";", " " };

            var words = data.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            double channelValue;
            Double.TryParse(words[1], out channelValue);

            //Debug.Log(words[1]);
            return channelValue;


        }

        public void DefineReader(string reader)
        {
            GameObject go = GameObject.Find(reader);
            _reader = (UDPReceiveBitalino)go.GetComponent(typeof(UDPReceiveBitalino));
            BufferSize = _reader.BufferSize;
            NumberOfBits = 0;
        }
    }

}