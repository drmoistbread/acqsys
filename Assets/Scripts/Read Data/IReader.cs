﻿using System;
using JetBrains.Annotations;
using Filter;

namespace Reader
{
    public interface IReader
    {
    string Name { get; set; }
    int BufferSize { get; }
    int NumberOfBits { get; set; }
    double[] GetBuffer(string channelToRead,FilterManager filterManager);
    void DefineReader(string reader);
    }

}