﻿using Devices;
using Players;
using System.Collections.Generic;
using Observer;
using UnityEngine;
using UnityEngine.UI;


namespace Events
{
    public class EventLogger : MonoBehaviour, IObserver
    {
        private const string EVENT_ROW_PREFAB_LOCATION = "Prefabs\\EventRow";

        public void Awake()
        {
            EventManager.GetInstance().SubscribeObserver(this);
        }
        public void Notify(object updatedData)
        {
            EventManager eventManager = (EventManager) updatedData;

            ClearPreviousRows();
            foreach (Event ev in eventManager.Events)
                CreateNewEventRow(ev.TimeOfOccuranceInSeconds, ev.Player, ev.EventMessage, ev.Device);
            
          

        }

        private void CreateNewEventRow(string time, Player player, string eventMessage, Device device)
        {
            GameObject newEventRow = (GameObject)Instantiate(Resources.Load(EVENT_ROW_PREFAB_LOCATION, typeof(GameObject)));

            Text lblTime = newEventRow.transform.FindChild("lbl_Time").GetComponent<Text>();
            lblTime.text = time;
            Text lblPlayer = newEventRow.transform.FindChild("lbl_Player").GetComponent<Text>();
            lblPlayer.text = player.Name;
            Text lblEvent = newEventRow.transform.FindChild("lbl_Event").GetComponent<Text>();
            lblEvent.text = eventMessage;
            Text lblDevice = newEventRow.transform.FindChild("lbl_Device").GetComponent<Text>();
            lblDevice.text = device.Name;

            GameObject eventScrollViewContent = GameObject.Find("ContentEvent");
            RectTransform eventScrollViewContentTransform = eventScrollViewContent.GetComponent<RectTransform>();
            eventScrollViewContentTransform.sizeDelta = new Vector2(eventScrollViewContentTransform.sizeDelta.x, 40 * EventManager.GetInstance().Events.Count+7);
            newEventRow.transform.SetParent(eventScrollViewContent.transform,false);
            


        }

        private void ClearPreviousRows()
        {
            GameObject[] eventRowsToDelete = GameObject.FindGameObjectsWithTag("EventRow");
            foreach (GameObject eventRow in eventRowsToDelete)
            {
                GameObject.Destroy(eventRow);
            }
        }

    }
}