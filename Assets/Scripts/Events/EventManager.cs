﻿using Devices;
using Observer;
using Players;
using System.Collections.Generic;


namespace Events
{
    public class EventManager : ISubject
    {
        private static EventManager _eventManager;
        private List<IObserver> _observers;
        internal List<Event> Events { private set; get; }

        public EventManager()
        {
            Events = new List<Event>();
            _observers = new List<IObserver>();
        }

        public static EventManager GetInstance()
        {
            if (_eventManager == null)
                _eventManager = new EventManager();
            return _eventManager;
        }
        public void AddNewEvent(Player player, Device device, string eventMessage, string timeInSeconds)
        {
            if (player == null || device == null || eventMessage == "")
                return;

            Event newEvent = new Event(player, device, eventMessage, timeInSeconds);
            Events.Add(newEvent);
            NotifyObservers(_eventManager);
        }
        #region Subject

        public void SubscribeObserver(IObserver observerToSubscribe)
        {
            _observers.Add(observerToSubscribe);
        }

        public void UnsubscribeObserver(IObserver observerToUnsubscribe)
        {
            _observers.Remove(observerToUnsubscribe);
        }

        public void UnsubcribeAllObservers()
        {
            _observers.Clear();
        }

        public void NotifyObservers(object updatedData)
        {
            foreach (IObserver observer in _observers)
            {
                observer.Notify(updatedData);
            }
        }
        #endregion
    }

}