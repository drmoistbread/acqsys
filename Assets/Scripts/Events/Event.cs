﻿using Devices;
using Players;



namespace Events
{
    internal class Event
    {


        public Device Device { get; set; }
        public Player Player { get; set; }
        public string EventMessage { get; set; }
        public string TimeOfOccuranceInSeconds { get; set; }

        public Event(Player player, Device device, string eventMessage, string timeInSeconds)
        {
            Player = player;
            Device = device;
            EventMessage = eventMessage;
            TimeOfOccuranceInSeconds = timeInSeconds;
        }

    }
}