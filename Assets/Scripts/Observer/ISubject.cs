﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Observer
{
    public interface ISubject
    {

        void SubscribeObserver(IObserver observerToSubscribe);

        void UnsubscribeObserver(IObserver observerToUnsubscribe);

        void NotifyObservers(System.Object updatedData);
    } 
}
