﻿using UnityEngine;
using System.Collections;


namespace Observer
{
    public interface IObserver
    {
        void Notify(System.Object updatedData);
    } 
}
