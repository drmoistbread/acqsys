﻿using System.Collections;
using System.Xml.Serialization;
using System.IO;
using System;
using System.Collections.Generic;
using Observer;
using Players;

namespace Signal
{
    public class SignalManager : ISubject
    {
        #region Declarations

        public SignalCollection Signals;

        private IList Observers { get; set; }

        #endregion

        public SignalManager()
        {
            Signals = new SignalCollection();
            Observers = new List<IObserver>();
        }

        public void AddSignal(ISignal signalToSave)
        {
            //I need to check if this is an update or just a new add.
            //so if s is null it's  new add, 
            //otherwise the easyest way to update is to remove the previous config for that channel and add the new one

            ISignal channelPreviousSave = GetSignal(signalToSave.Name);

            if (channelPreviousSave == null)
            {
                Signals.Add(signalToSave);
            }
            else
            {
                Signals.Remove(channelPreviousSave);
                Signals.Add(signalToSave);
            }



            PlayerManager.GetInstance().NotifyObservers(PlayerManager.GetInstance());

        }



        public ISignal GetSignal(string channelName)
        {
            foreach (ISignal signal in Signals)
                if (channelName == signal.Name)
                    return signal;

            return null;
        }

        public void DeleteSignal(string channelName)
        {
            foreach (ISignal signal in Signals)
                if (channelName == signal.Name)
                {
                    Signals.Remove(signal);
                    PlayerManager.GetInstance().NotifyObservers(PlayerManager.GetInstance());
                    return;

                }

        }

        public bool SignalExists(string channelName)
        {
            foreach (ISignal signal in Signals)
                if (channelName == signal.Name)
                    return true;

            return false;
        }

        public void SaveChannelsConfigurationToXml(string path)
        {
            var serializer = new XmlSerializer(typeof(SignalManager), new Type[] { typeof(SignalCollection) });
            FileStream fs = new FileStream(path, FileMode.Create);

            serializer.Serialize(fs, this);
            fs.Close();

        }

        public void LoadChannelsConfigurationFromXml(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open);

            XmlSerializer serializer = new XmlSerializer(typeof(SignalManager), new Type[] { typeof(SignalCollection) });

            Signals = ((SignalManager)serializer.Deserialize(fs)).Signals;
            //this = (SignalManager)serializer.Deserialize(fs);

            fs.Close();

            NotifyObservers(PlayerManager.GetInstance());

        }

        //this is the subject for the signal buttons
        #region Subject

        public void SubscribeObserver(IObserver observerToSubscribe)
        {
            Observers.Add(observerToSubscribe);
        }

        public void UnsubscribeObserver(IObserver observerToUnsubscribe)
        {
            Observers.Remove(observerToUnsubscribe);
        }

        public void NotifyObservers(System.Object updatedData)
        {
            foreach (IObserver observer in Observers)
            {
                observer.Notify(updatedData);
            }
        }



        #endregion
    }

}