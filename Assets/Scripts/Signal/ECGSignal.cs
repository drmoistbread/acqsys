﻿using Filter;
using System;
using UnityEngine;
using System.Xml.Serialization;
using Reader;

namespace Signal
{
    public class ECGSignal : ISignal
    {

        public string ChannelNumber { get; set; }
        public string Name { get; set; }
        public string Range { get; set; }
        public string Gain { get; set; }
        public string Units { get; set; }
        public string Bandwidth { get; set; }
        public string Impedance { get; set; }
        public string Cmrr { get; set; }
        public string DeviceName { get; set; }
        public float ScaleX { get; set; }
        public float ScaleY { get; set; }
        [XmlIgnore]
        public IReader Reader { get; set; }
        [XmlIgnore]
        public FilterManager FilterManager { get; set; }

        public ECGSignal()
        {
            ScaleX = 1;
            ScaleY = 1;
            FilterManager = new FilterManager();
        }
        public override string ToString()
        {
            return "Channelnumber: " + ChannelNumber +
                   " Name: " + Name +
                   " Range: " + Range +
                   " Gain: " + Gain +
                   " Units: " + Units +
                   " Bandwidth: " + Bandwidth +
                   " Impedance: " + Impedance +
                   " Cmrr: " + Cmrr;
        }

        public double[] GetBuffer()
        {
            return Reader.GetBuffer(ChannelNumber, FilterManager);
        }

        public float Calculate()
        {
            throw new NotImplementedException();
        }
    }


}