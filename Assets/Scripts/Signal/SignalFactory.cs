﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Signal
{
    public static class SignalFactory
    {


        public static ISignal SetUpCorrectSignalType(string signalType)
        {
            switch (signalType)
            {
                case "ECG":
                    return new ECGSignal();
                case "PPG":
                    return new PPGSignal();
                case "EDA":
                    return new EDASignal();
                case "ACC":
                    return new ACCSignal();
                default:
                    return null;

            }
        }
    }

}