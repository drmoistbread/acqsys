﻿using Filter;
using System.Xml.Serialization;
using Reader;

namespace Signal
{
    public interface ISignal
    {
     string ChannelNumber { get; set; }
     string Name { get; set; }
     string Range { get; set; }
     string Gain { get; set; }
     string Units { get; set; }
     string Bandwidth { get; set; }
     string Impedance { get; set; }
     string Cmrr { get; set; }
     string DeviceName { get; set; }
     float ScaleX { get; set; }
     float ScaleY { get; set; }
     FilterManager FilterManager { get; set; }

    [XmlIgnore]
    IReader Reader { get; set; }

    double[] GetBuffer();
    float Calculate();
    string ToString();

    }

}