﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Reader;
using UnityEngine;

namespace Signal
{
    public class SignalCollection : ICollection<ISignal>, IXmlSerializable
    {
        #region Collection

        private IList<ISignal> _signals;

        public SignalCollection()
        {
            _signals = new List<ISignal>();
        }

        public IEnumerator<ISignal> GetEnumerator()
        {
            return _signals.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(ISignal item)
        {
            _signals.Add(item);
        }

        public void Clear()
        {
            _signals.Clear();
        }

        public bool Contains(ISignal item)
        {
            return _signals.Contains(item);
        }

        public void CopyTo(ISignal[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(ISignal item)
        {
            return _signals.Remove(item);
        }

        public int Count
        {
            get { return _signals.Count; }
        }

        public bool IsReadOnly
        {
            get { return _signals.IsReadOnly; }
        }

        #endregion

        #region XMLSerialization

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            _signals.Clear();
            try
            {
                reader.Read();
                //let's go through all the xml file and break when we arrive at Signals closing tag.
                while (true)
                {
                    Debug.Log("@" + reader.Name);
                    if (reader.Name.ToLower() == "signals")
                        break;

                    ISignal deserializedSignal;
                    switch (reader.Name.ToLower())
                    {
                        case "ecgsignal":
                            deserializedSignal = DeserializeSignal(typeof(ECGSignal), reader);
                            _signals.Add(deserializedSignal);
                            break;
                        case "accsignal":
                            deserializedSignal = DeserializeSignal(typeof(ACCSignal), reader);
                            _signals.Add(deserializedSignal);
                            break;
                        case "edasignal":
                            deserializedSignal = DeserializeSignal(typeof(EDASignal), reader);
                            _signals.Add(deserializedSignal);
                            break;
                        case "ppgsignal":
                            deserializedSignal = DeserializeSignal(typeof(PPGSignal), reader);
                            _signals.Add(deserializedSignal);
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var signal in _signals)
            {
                XmlSerializer serializer = new XmlSerializer(signal.GetType());

                serializer.Serialize(writer, signal);
                serializer = null;
            }
        }

        private ISignal DeserializeSignal(Type typeOfSignal, XmlReader reader)
        {
            XmlSerializer serializer = new XmlSerializer(typeOfSignal);
            ISignal deserializedSignal = (ISignal)serializer.Deserialize(reader);
            string platform = deserializedSignal.DeviceName;
            deserializedSignal.Reader = ReaderFactory.CreateNewReader(platform);
            return deserializedSignal;
        }

        #endregion

    }

}