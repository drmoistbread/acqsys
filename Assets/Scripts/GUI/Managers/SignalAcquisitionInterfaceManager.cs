﻿using Devices;
using DrawSignals;
using Elements.Toggle;
using Observer;
using Players;
using Reader;
using Signal;
using System;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SignalAcquisitionInterfaceManager : MonoBehaviour, IObserver
{
    #region Declarations

    public string _device;

    private PlayerManager Players;

    public Dropdown ddl_SignalType,
        ddl_Player,
        ddl_PlayerNames,
        ddl_DeviceReader,
        ddl_PlayerChannel,
        ddl_DeviceChannel;


    public InputField txt_ChannelName,
        txt_ChannelNumber,
        txt_DeviceName,
        txt_PlayerName,
        txt_ChannelConnector;

    //this is for the dynamic toggle created when a new signal is added
    private DynamicToggleManager _toggleManager;

    public Text lbl_ActiveChannel;

    private enum _signaltype
    {
        ECGSignal,
        PPGSignal,
        EDASignal,
        EMGSignal,
        EEGSignal,
        ACCSignal,
        LUXSignal
    }

    #endregion

    void Awake()
    {
        Players = PlayerManager.GetInstance();
        DynamicToggleManager.CreateInstance(Globals.CHANNEL_BUTTON_X, Globals.CHANNEL_BUTTON_Y, Players, this);
        LoadDeviceReaders();
        Players.NotifyObservers(Players);
    }

    private void LoadDeviceReaders()
    {
        string[] deviceReaderStringNames = { "BITalino", "BITalino-CP" };
        ddl_DeviceReader.AddOptions(deviceReaderStringNames.ToList());
    }

    public void SaveChannelConfiguration()
    {
        if (ddl_PlayerChannel.value == 0 || ddl_DeviceChannel.value == 0)
            return;

        string playerName = ddl_PlayerChannel.captionText.text;

        SignalManager signals = Players.GetPlayer(playerName).Signals;
        Device device = DeviceManager.GetInstance().GetDevice(ddl_DeviceChannel.captionText.text);

        ISignal signalToSave = SignalFactory.SetUpCorrectSignalType(ddl_SignalType.captionText.text);

        signalToSave.ChannelNumber = txt_ChannelNumber.text;

        signalToSave.Name = playerName + "-" + Globals.GetSignalFromChannelName(txt_ChannelName.text);

        signalToSave.Reader = device.DeviceReader;

        signalToSave.DeviceName = device.Name;

        signals.AddSignal(signalToSave);

        ResetChannelConfiguration();

    }

    public void DeleteChannelConfiguration()
    {
        string playerName = Globals.GetPlayerFromChannelName(txt_ChannelName.text);

        Players.GetPlayer(playerName).Signals.DeleteSignal(txt_ChannelName.text);
        DrawSignalManager.GetInstance().DestroyAllGraphLines();
        ResetChannelConfiguration();
    }

    public void ResetChannelConfiguration()
    {
        txt_ChannelNumber.text = "";
        txt_ChannelName.text = "";
        ddl_SignalType.value = 0;
        ddl_DeviceChannel.value = 0;
        ddl_PlayerChannel.value = 0;
    }

    public void AddPlayer()
    {
        if (ddl_PlayerNames.value == 0)
            AddNewPlayer();
        else
            EditPlayer();

    }

    public void AddNewPlayer()
    {
        PlayerManager playerManager = PlayerManager.GetInstance();

        string playerName = txt_PlayerName.text;

        playerManager.AddPlayer(new Player(playerName));

        Dropdown.OptionData newPlayerNameOption = new Dropdown.OptionData();
        newPlayerNameOption.text = playerName;
        ddl_PlayerNames.options.Add(newPlayerNameOption);

        playerManager.NotifyObservers(playerManager);

        txt_PlayerName.text = "";
    }

    public void EditPlayer()
    {
        PlayerManager playerManager = PlayerManager.GetInstance();

        string playerToEditName = ddl_PlayerNames.captionText.text;

        Player playerToEdit = playerManager.GetPlayer(playerToEditName);

        playerToEdit.Name = txt_PlayerName.text;

        ddl_PlayerNames.options.RemoveAt(ddl_PlayerNames.value);

        Dropdown.OptionData newPlayerNameOption = new Dropdown.OptionData();
        newPlayerNameOption.text = txt_PlayerName.text;
        ddl_PlayerNames.options.Add(newPlayerNameOption);

        playerManager.NotifyObservers(playerManager.GetAllPlayersAsString());

        txt_PlayerName.text = "";

    }

    public void DeletePlayer()
    {
        if (ddl_PlayerNames.value == 0)
            return;

        string playerNameToDelete = ddl_PlayerNames.captionText.text;

        PlayerManager.GetInstance().DeletePlayer(playerNameToDelete);

        ddl_PlayerNames.options.RemoveAt(ddl_PlayerNames.value);

        PlayerManager.GetInstance().NotifyObservers(PlayerManager.GetInstance());
    }

    public void AddNewDevice()
    {
        if (ddl_DeviceReader.value == 0)
            return;

        string reader = ddl_DeviceReader.captionText.text;

        DeviceManager devices = DeviceManager.GetInstance();

        Device dev = Device.CreateInstance( /*name = */txt_DeviceName.text,
            /*reader = */ ReaderFactory.CreateNewReader(reader), /* channelConnector = */txt_ChannelConnector.text);
        devices.AddDevice(dev);

        GameObject deviceGo = (GameObject)Instantiate(Resources.Load("Prefabs/Devices/" + reader, typeof(GameObject)));

        deviceGo.name = txt_DeviceName.text;
        dev.SetDeviceGameObject(deviceGo);
        dev.DeviceReader.DefineReader(dev.Name);

        ResetDeviceConfiguration();
    }


    public void ResetDeviceConfiguration()
    {
        txt_DeviceName.text = "";
        txt_ChannelConnector.text = "";
        ddl_DeviceReader.value = 0;
    }

    public void SaveChannelConfigurationToXml()
    {
        CustomFileExplorerManager.GetInstance().ShowSaveFileWindow(fp =>
        {
            DeviceManager.GetInstance().SaveDevicesConfigurationToXml(fp + ".xml", FileMode.Create);
            PlayerManager.GetInstance().SaveChannelsConfigurationToXml(fp + ".xml-p", FileMode.Create);
        });
    }

    public void LoadChannelConfigurationFromXml()
    {
        CustomFileExplorerManager.GetInstance().ShowOpenFileWindow(fp =>
         {
             DeviceManager.GetInstance().LoadDevicesConfigurationFromXml(fp);
             Players.LoadChannelsConfigurationFromXml(fp + "-p");
             Players.SubscribeObserver(_toggleManager);
         });
    }

    public void LoadSignalProcessingScene()
    {
        PlayerManager.GetInstance().UnsubscribeAllObservers();
        SceneManager.LoadScene("SignalProcessing");
    }

    #region Observer

    //this is onClick action for the dymanic signal Toggle
    public void Notify(object updatedData)
    {

        string channelNameToLoad = (string)updatedData;

        GameObject toggleClicked = GameObject.Find(channelNameToLoad);
        Toggle toggleComponent = (Toggle)toggleClicked.GetComponent(typeof(Toggle));

        string playerName = Globals.GetPlayerFromChannelName(channelNameToLoad);
        Player player = Players.GetPlayer(playerName);
        ISignal signal = player.Signals.GetSignal(channelNameToLoad);

        if (signal == null)
            return;

        if (toggleComponent.isOn == false)
        {
            DrawSignalManager.GetInstance().DestroyGraphLine(signal);
            return;
        }

        txt_ChannelNumber.text = signal.ChannelNumber;
        txt_ChannelName.text = signal.Name;

        ddl_SignalType.value = GetSignalTypeValue(signal);
        ddl_DeviceChannel.value = DeviceManager.GetInstance().GetDeviceIndex(signal.DeviceName) + 1;
        ddl_PlayerChannel.value = Players.GetPlayerIndex(playerName) + 1;

        lbl_ActiveChannel.text = signal.Name;

        DrawSignalManager.GetInstance().CreateNewGraphLine(player.Colour, signal, Globals.GRAPH_INITIAL_X, Globals.GRAPH_INITIAL_Y);

    }

    private static int GetSignalTypeValue(ISignal signal)
    {
        return (int)Enum.Parse(typeof(_signaltype), signal.GetType().FullName.Split('.')[1]);
    }

    #endregion

}
