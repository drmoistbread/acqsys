﻿using Devices;
using DrawSignals;
using Elements.Toggle;
using Events;
using Observer;
using Players;
using Signal;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SignalVisualizationInterfaceManager : MonoBehaviour, IObserver
{
    public PlayerManager Players { get; private set; }
    public DeviceManager Devices { get; private set; }
    public EventManager Events { get; set; }

    public Text lblActiveChannel;

    public Dropdown ddlEventPlayer,
        ddlEventDevice;

    public InputField txtEventMessage;
    private bool lol = true;
    private Clock clc;

    private GameObject eventLoggingPanel;
    private int _timeOfOccurrenceLastEvent;

    // Use this for initialization
    void Awake()
    {
        Players = PlayerManager.GetInstance();
        Devices = DeviceManager.GetInstance();
        Events = EventManager.GetInstance();

        DynamicToggleManager.CreateInstance(Globals.CHANNEL_BUTTON_X, Globals.CHANNEL_BUTTON_Y, Players, this);

        Devices.NotifyObservers(Devices);
        Players.NotifyObservers(Players);
        Events.NotifyObservers(Events);
        clc = new Clock("Testing.txt");
        eventLoggingPanel = GameObject.Find("EventLoggingPannel");
        eventLoggingPanel.SetActive(false);
    }

    public void LoadSignalAcquisiitionScene()
    {
        Players.UnsubscribeAllObservers();
        Events.UnsubcribeAllObservers();
        SceneManager.LoadScene("SignalProcessing");
    }


    public void StartRecording()
    {
        if (lol)
        {

            clc.Start();
            lol = false;
        }
        else
        {
            clc.Stop();
            lol = true;
        }
        Debug.Log(clc.IsRecording);


    }

    public void AddNewEvent()
    {
        if (!clc.IsRecording)
            return;

        if (eventLoggingPanel.activeSelf)
        {

            Player eventPlayer = Players.GetPlayer(ddlEventPlayer.captionText.text);
            string eventMessage = txtEventMessage.text;
            Device eventDevice = Devices.GetDevice(ddlEventDevice.captionText.text);

            Events.AddNewEvent(eventPlayer, eventDevice, eventMessage, _timeOfOccurrenceLastEvent.ToString());
            eventLoggingPanel.SetActive(false);
        }
        else
        {
            _timeOfOccurrenceLastEvent = clc.TimeElapsedInSeconds;
            eventLoggingPanel.SetActive(true);
        }
    }



    #region Observer
    public void Notify(object updatedData)
    {
        string channelNameToLoad = (string)updatedData;

        string playerName = Globals.GetPlayerFromChannelName(channelNameToLoad);
        Player player = Players.GetPlayer(playerName);
        ISignal signal = player.Signals.GetSignal(channelNameToLoad);

        if (signal == null)
            return;

        lblActiveChannel.text = signal.Name;

        DrawSignalManager.GetInstance().CreateNewGraphLine(player.Colour, signal, Globals.GRAPH_INITIAL_X, Globals.GRAPH_INITIAL_Y);
    }
    #endregion
}
