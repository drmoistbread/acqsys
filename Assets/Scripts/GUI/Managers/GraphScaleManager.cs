﻿using UnityEngine;
using System.Collections;
using DrawSignals;
using Players;
using Signal;
using UnityEngine.UI;

public class GraphScaleManager : MonoBehaviour
{

    #region Declarations

    private const float
        POSITION_X = 5f,
        POSITION_Y = 5f,
        SCALE_X = 10f,
        SCALE_Y = 2f;
    private float _lineThickness = 2f;

    public Text lbl_ActiveChannel;

    private PlayerManager Players;

    #endregion

    public void Start()
    {
        Players = PlayerManager.GetInstance();
    }

    public void PositionXChangedRight()
    {
        string playerName = Globals.GetPlayerFromChannelName(lbl_ActiveChannel.text);
        SignalManager signals = Players.GetPlayer(playerName).Signals;

        ISignal signalGraphToManipulate = signals.GetSignal(lbl_ActiveChannel.text);
        if (signalGraphToManipulate != null)
            DrawSignalManager.GetInstance().UpdateGraphXPosition(signalGraphToManipulate, POSITION_X);


    }

    public void PositionXChangeLeft()
    {
        string playerName = Globals.GetPlayerFromChannelName(lbl_ActiveChannel.text);
        SignalManager signals = Players.GetPlayer(playerName).Signals;

        ISignal signalGraphToManipulate = signals.GetSignal(lbl_ActiveChannel.text);
        if (signalGraphToManipulate != null)
            DrawSignalManager.GetInstance().UpdateGraphXPosition(signalGraphToManipulate, -POSITION_X);


    }

    public void PositionYChangedUp()
    {
        string playerName = Globals.GetPlayerFromChannelName(lbl_ActiveChannel.text);
        SignalManager signals = Players.GetPlayer(playerName).Signals;

        ISignal signalGraphToManipulate = signals.GetSignal(lbl_ActiveChannel.text);
        if (signalGraphToManipulate != null)
            DrawSignalManager.GetInstance().UpdateGraphYPosition(signalGraphToManipulate, POSITION_Y);

    }

    public void PositionYChangedDown()
    {
        string playerName = Globals.GetPlayerFromChannelName(lbl_ActiveChannel.text);
        SignalManager signals = Players.GetPlayer(playerName).Signals;

        ISignal signalGraphToManipulate = signals.GetSignal(lbl_ActiveChannel.text);
        if (signalGraphToManipulate != null)
            DrawSignalManager.GetInstance().UpdateGraphYPosition(signalGraphToManipulate, -POSITION_Y);

    }

    public void ScaleXChanged()
    {
        // BitalinoReader reader = GameObject.Find(Globals.BITALINO_GAMEOBJECT_NAME).GetComponent<BitalinoReader>();

    }

    public void ScaleYChangedUp()
    {
        string playerName = Globals.GetPlayerFromChannelName(lbl_ActiveChannel.text);
        SignalManager signals = Players.GetPlayer(playerName).Signals;

        ISignal signalGraphToManipulate = signals.GetSignal(lbl_ActiveChannel.text);
        if (signalGraphToManipulate != null)
            DrawSignalManager.GetInstance().UpdateGraphYScale(signalGraphToManipulate, SCALE_Y);
    }

    public void ScaleYChangedDown()
    {
        string playerName = Globals.GetPlayerFromChannelName(lbl_ActiveChannel.text);
        SignalManager signals = Players.GetPlayer(playerName).Signals;

        ISignal signalGraphToManipulate = signals.GetSignal(lbl_ActiveChannel.text);
        if (signalGraphToManipulate != null)
            DrawSignalManager.GetInstance().UpdateGraphYScale(signalGraphToManipulate, 1 / SCALE_Y);
    }

    public void LineThicknessPlus()
    {
        string playerName = Globals.GetPlayerFromChannelName(lbl_ActiveChannel.text);
        SignalManager signals = Players.GetPlayer(playerName).Signals;

        ISignal signalGraphToManipulate = signals.GetSignal(lbl_ActiveChannel.text);
        if (signalGraphToManipulate != null)
        {
            _lineThickness += .5f;
            DrawSignalManager.GetInstance().UpdateLineThickness(signalGraphToManipulate, _lineThickness);
        }
    }
    public void LineThicknessMinus()
    {
        string playerName = Globals.GetPlayerFromChannelName(lbl_ActiveChannel.text);
        SignalManager signals = Players.GetPlayer(playerName).Signals;

        ISignal signalGraphToManipulate = signals.GetSignal(lbl_ActiveChannel.text);
        if (signalGraphToManipulate != null)
        {
            _lineThickness -= .5f;
            DrawSignalManager.GetInstance().UpdateLineThickness(signalGraphToManipulate, _lineThickness);
        }
    }


}
