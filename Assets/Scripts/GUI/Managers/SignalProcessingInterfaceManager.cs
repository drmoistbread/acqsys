﻿using DrawSignals;
using Elements.Toggle;
using Observer;
using Players;
using Signal;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SignalProcessingInterfaceManager : MonoBehaviour, IObserver
{
    public PlayerManager Players { get; private set; }
    public Text lbl_ActiveChannel;



    // Use this for initialization
    void Start()
    {
        Players = PlayerManager.GetInstance();
        //DeviceManager.GetInstance();

        //DynamicToggleManager toggleManager = new DynamicToggleManager(Globals.CHANNEL_BUTTON_X, Globals.CHANNEL_BUTTON_Y, Players, this);
        DynamicToggleManager.CreateInstance(Globals.CHANNEL_BUTTON_X, Globals.CHANNEL_BUTTON_Y, Players, this);

        Players.NotifyObservers(Players);
    }

    public void LoadSignalAcquisiitionInterface()
    {
        PlayerManager.GetInstance().UnsubscribeAllObservers();
        SceneManager.LoadScene("SignalAquisition");
    }
    public void LoadSignalVisialisationInterface()
    {
        PlayerManager.GetInstance().UnsubscribeAllObservers();
        SceneManager.LoadScene("SignalVisualisation");
    }




    private void UncheckAllFilters()
    {
        GameObject[] toggles = GameObject.FindGameObjectsWithTag("Filter");
        foreach (GameObject toggle in toggles)
        {
            toggle.GetComponent<Toggle>().isOn = false;
        }
    }

    #region Observer
    public void Notify(object updatedData)
    {
        string channelNameToLoad = (string)updatedData;

        GameObject toggleClicked = GameObject.Find(channelNameToLoad);
        Toggle toggleComponent = (Toggle)toggleClicked.GetComponent(typeof(Toggle));

        string playerName = Globals.GetPlayerFromChannelName(channelNameToLoad);
        Player player = Players.GetPlayer(playerName);
        ISignal signal = player.Signals.GetSignal(channelNameToLoad);

        if (signal == null)
            return;

        if (toggleComponent.isOn == false)
        {
            DrawSignalManager.GetInstance().DestroyGraphLine(signal);
            FilterSlider.HideAllSliders();
            UncheckAllFilters();
            return;
        }

        lbl_ActiveChannel.text = signal.Name;

        DrawSignalManager.GetInstance().CreateNewGraphLine(player.Colour, signal, Globals.GRAPH_INITIAL_X, Globals.GRAPH_INITIAL_Y);
    }
    #endregion
}
