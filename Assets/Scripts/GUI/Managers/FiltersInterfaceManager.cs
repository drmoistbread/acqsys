﻿using Players;
using Signal;
using UnityEngine;
using UnityEngine.UI;


public class FiltersInterfaceManager : MonoBehaviour
{

    #region Declarations
    public Text txt_ActiveChannel;

    public Toggle cbHighPass,
        cbLowPass;
    #endregion
    // Use this for initialization


    public void HighPassFilter()
    {
        string highPassFilterText = cbHighPass.GetComponentInChildren<Text>().text;

        if (cbHighPass.isOn)
        {
            FilterSlider.ShowSlider(highPassFilterText);

            ISignal activeChannel = RetrieveActiveChannel();
            activeChannel.FilterManager.AddNewFilter("highpassfilter");

            Slider slider = GameObject.Find(highPassFilterText).GetComponentInChildren<Slider>();
            slider.onValueChanged.AddListener(v =>
            {
                activeChannel.FilterManager.SetFilterValue("highpassfilter", v);
            });
        }
        else
        {
            FilterSlider.HideSlider(highPassFilterText);

            ISignal activeChannel = RetrieveActiveChannel();
            activeChannel.FilterManager.UnsetFilter("highpassfilter");

        }
    }
    public void LowPassFilter()
    {
        string lowPassFilterText = cbLowPass.GetComponentInChildren<Text>().text;
        if (cbLowPass.isOn)
        {
            FilterSlider.ShowSlider(lowPassFilterText);

            ISignal activeChannel = RetrieveActiveChannel();
            activeChannel.FilterManager.AddNewFilter("lowpassfilter");

            Slider slider = GameObject.Find(lowPassFilterText).GetComponentInChildren<Slider>();
            slider.onValueChanged.AddListener(v =>
            {
              activeChannel.FilterManager.SetFilterValue("lowpassfilter",v);
            });
        }
        else
        {
            FilterSlider.HideSlider(lowPassFilterText);

            ISignal activeChannel = RetrieveActiveChannel();
            activeChannel.FilterManager.UnsetFilter("lowpassfilter");

        }
    }

    private ISignal RetrieveActiveChannel()
    {
        string playerName = Globals.GetPlayerFromChannelName(txt_ActiveChannel.text);
        string signalName = Globals.GetSignalFromChannelName(txt_ActiveChannel.text);

        return PlayerManager.GetInstance().GetPlayer(playerName).Signals.GetSignal(signalName);
    }
}
