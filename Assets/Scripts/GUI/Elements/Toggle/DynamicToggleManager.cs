﻿using Observer;
using Players;
using Signal;
using System.Collections.Generic;
using UnityEngine;
namespace Elements.Toggle
{

    public class DynamicToggleManager : ScriptableObject, IObserver
    {
        #region Declararions

        private float _xPosition,
            _yPosition;

        private const float PADDING = 40.0f;
        private IList<DynamicToggle> _dynamicButtons;
        private IObserver _interfaceManager;
        private RectTransform _content;

        #endregion

        public static DynamicToggleManager CreateInstance(float x, float y, ISubject playerManager, IObserver interfaceManager)
        {
            DynamicToggleManager newToggleManagerInstance = CreateInstance<DynamicToggleManager>();
            newToggleManagerInstance.Initialize(x,y,playerManager,interfaceManager);

            return newToggleManagerInstance;
        }

        private void Initialize(float x, float y, ISubject playerManager, IObserver interfaceManager)
        {
            GameObject verticalScrollView = (GameObject)Instantiate(Resources.Load("Prefabs\\VerticalScrollView", typeof(GameObject)));

            Canvas canvas = (Canvas)GameObject.Find("Canvas").GetComponent<Canvas>();

            _content = (RectTransform)GameObject.Find("Content").GetComponent(typeof(RectTransform));

            verticalScrollView.transform.SetParent(canvas.transform, false);
            verticalScrollView.transform.localPosition = new Vector3(x, y, 0);


            _dynamicButtons = new List<DynamicToggle>();
            playerManager.SubscribeObserver(this);
            _interfaceManager = interfaceManager;
        }

        private void AddToggle(string channelName, IObserver observer)
        {
            DynamicToggle newToggle = DynamicToggle.CreateInstance(channelName, observer);
            _dynamicButtons.Add(newToggle);
            _content.sizeDelta = new Vector2(_content.sizeDelta.x, 25 * _dynamicButtons.Count);

        }

        private DynamicToggle GetToggle(string channelName)
        {

            foreach (DynamicToggle button in _dynamicButtons)
                if (button.GetChannelName() == channelName)
                    return button;
            return null;

        }

        private void DeleteToggle(string channelName)
        {
            foreach (DynamicToggle toggle in _dynamicButtons)
                if (toggle.GetChannelName() == channelName)
                {
                    toggle.Delete();
                    return;
                }
        }

        public void UnsubscribeObserver()
        {
            PlayerManager.GetInstance().UnsubscribeObserver(this);
        }

        #region Observer

        public void Notify(System.Object updatedData)
        {

            PlayerManager players = (PlayerManager)updatedData;

            foreach (DynamicToggle button in _dynamicButtons)
            {
                DeleteToggle(button.GetChannelName());
            }
            _dynamicButtons.Clear();

            foreach (Player player in players.GetAllPlayers())
            {
                foreach (ISignal signal in player.Signals.Signals)
                {
                    AddToggle(signal.Name, _interfaceManager);
                    GetToggle(signal.Name).SetTextColour(player.Colour);
                }
            }
        }

        #endregion
    }

}