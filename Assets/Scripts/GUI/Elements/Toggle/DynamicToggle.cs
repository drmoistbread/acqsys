﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Observer;
using UnityEngine.UI;


namespace Elements.Toggle
{
    class DynamicToggle : ScriptableObject, ISubject
    {
        private  GameObject _ToggleGO;
        private  Text _name;
        private  UnityEngine.UI.Toggle _checkbox;

        private const int TOGGLE_WIDTH = 150,
            TOGGLE_HEIGHT = 25;
        private IList _observers;

        public static DynamicToggle CreateInstance(string channelName, IObserver interfaceManager)
        {
            DynamicToggle newDynamicToggleInstance = CreateInstance<DynamicToggle>();
            newDynamicToggleInstance.Initialize(channelName,interfaceManager);
            return newDynamicToggleInstance;
        }
        public void Initialize(string channelName, IObserver interfaceManager)
        {
            _ToggleGO = (GameObject)Instantiate(Resources.Load("Prefabs/Checkbox", typeof(GameObject)));
            _ToggleGO.name = channelName;

            _name = (Text)_ToggleGO.GetComponentInChildren(typeof(Text));
            _name.text = channelName;

            _checkbox = (UnityEngine.UI.Toggle)_ToggleGO.GetComponent(typeof(UnityEngine.UI.Toggle));
            _checkbox.isOn = false;

            GameObject content = GameObject.Find("Content");

            _ToggleGO.transform.SetParent(content.transform, false);

            //I dont set a position because I use the function setPosition
            //The DynamicToggleManager is the one responsible to define the position
            _ToggleGO.transform.localPosition = new Vector3(0, 0, 0);

            _observers = new List<IObserver>();

            SubscribeObserver(interfaceManager);

            _checkbox.onValueChanged.AddListener(delegate
            {
                ToggleClicked(channelName);
            }
            );
        }

        void ToggleClicked(string channelName)
        {
            NotifyObservers(channelName);
        }

        public void SetPosition(float x, float y, float z)
        {
            _ToggleGO.transform.localPosition = new Vector3(x, y, z);
        }

        public string GetChannelName()
        {
            return _name.text;

        }

        public void SetTextColour(Color colour)
        {
            _name.color = colour;
        }

        public void Delete()
        {
            GameObject.Destroy(_ToggleGO);
        }

        #region Subject

        public void SubscribeObserver(IObserver observerToSubscribe)
        {
            _observers.Add(observerToSubscribe);
        }

        public void UnsubscribeObserver(IObserver observerToUnsubscribe)
        {
            _observers.Remove(observerToUnsubscribe);
        }

        public void NotifyObservers(System.Object updatedData)
        {
            foreach (IObserver observer in _observers)
            {
                observer.Notify(updatedData);
            }
        }

        #endregion

    } 
}
