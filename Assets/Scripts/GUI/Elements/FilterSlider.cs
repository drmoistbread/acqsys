﻿using UnityEngine;
using UnityEngine.UI;

public class FilterSlider : MonoBehaviour
{

    private const float X_POSITION = -400,
        Y_POSITION = -300;


    private const string FILTER_PREFAB = "Prefabs\\FilterSlider";
    public static void ShowSlider(string sliderName, float minimumSliderValue, float maximumSliderValue, float xPosition, float yPosition)
    {
        GameObject newFilterSlider = (GameObject)GameObject.Instantiate(Resources.Load(FILTER_PREFAB));
        newFilterSlider.name = sliderName;
        Text sliderLabel = newFilterSlider.GetComponentInChildren<Text>();
        sliderLabel.text = sliderName;

        GameObject canvas = GameObject.Find("Canvas");
        newFilterSlider.transform.SetParent(canvas.transform);

        Slider slider = newFilterSlider.GetComponentInChildren<Slider>();
        slider.minValue = minimumSliderValue;
        slider.maxValue = maximumSliderValue;

        newFilterSlider.transform.localPosition = new Vector3(xPosition, yPosition);
    }
    public static void ShowSlider(string sliderName, float minimumSliderValue, float maximumSliderValue)
    {
        GameObject newFilterSlider = (GameObject)GameObject.Instantiate(Resources.Load(FILTER_PREFAB));
        newFilterSlider.name = sliderName;
        Text sliderLabel = newFilterSlider.GetComponentInChildren<Text>();
        sliderLabel.text = sliderName;

        GameObject canvas = GameObject.Find("Canvas");
        newFilterSlider.transform.SetParent(canvas.transform);

        Slider slider = newFilterSlider.GetComponentInChildren<Slider>();
        slider.minValue = minimumSliderValue;
        slider.maxValue = maximumSliderValue;

        slider.onValueChanged.AddListener(v =>
        {
            Debug.Log("my delegate" + v);
        });
        newFilterSlider.transform.localPosition = new Vector3(X_POSITION, Y_POSITION);

    }
    public static void ShowSlider(string sliderName)
    {
        GameObject newFilterSlider = (GameObject)GameObject.Instantiate(Resources.Load(FILTER_PREFAB));
        newFilterSlider.name = sliderName;
        Text sliderLabel = newFilterSlider.GetComponentInChildren<Text>();
        sliderLabel.text = sliderName;

        Text label = newFilterSlider.GetComponentInChildren<Text>();
        label.text = sliderName;

        GameObject canvas = GameObject.Find("Canvas");
        newFilterSlider.transform.SetParent(canvas.transform);
        newFilterSlider.transform.localPosition = new Vector3(X_POSITION, Y_POSITION);

    }
    public static void HideSlider(string sliderName)
    {
        GameObject.Destroy(GameObject.Find(sliderName));
    }

    public static void HideAllSliders()
    {
        GameObject[] sliders = GameObject.FindGameObjectsWithTag("Slider");
        foreach (GameObject slider in sliders)
        {
            GameObject.Destroy(slider);
        }
    }
}
