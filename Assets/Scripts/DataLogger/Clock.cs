﻿using System;
using System.Timers;
using UnityEngine;

public class Clock
{

    private readonly Timer _timer;
    private DataLogger _dataLogger;
    private string _txtFilePath;
    public int TimeElapsedInSeconds { get; private set; }

    public bool IsRecording
    {
        get { return _timer.Enabled; }
    }


    public Clock(string txtFilePath)
    {
        _txtFilePath = txtFilePath;
        TimeElapsedInSeconds = 0;
        _timer = new Timer { Interval = 1000 };
        _timer.Elapsed += OnTimedEvent;
    }

    public void Start()
    {
        string desktopDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        _dataLogger = new DataLogger(desktopDir + "\\" + _txtFilePath);
        _timer.Start();
    }
    public void Stop()
    {
        _timer.Stop();
        TimeElapsedInSeconds = 0;
        _dataLogger.CloseStream();
    }

    private void OnTimedEvent(object source, ElapsedEventArgs e)
    {
        TimeElapsedInSeconds++;
        _dataLogger.WriteToFile(TimeElapsedInSeconds.ToString());
        Debug.Log(TimeElapsedInSeconds + "  " + this.GetType().ToString());

    }


}
