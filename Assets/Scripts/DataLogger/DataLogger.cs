﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataLogger
{
    private readonly StreamWriter _writer;

    public DataLogger(string txtPathName)
    {
        _writer = new StreamWriter(txtPathName);
        _writer.AutoFlush = true;
    }

    public void WriteToFile(string lol)
    {
        _writer.WriteLine(lol);
        //_writer.Flush();
    }

    public void CloseStream()
    {
        _writer.Dispose();
    }
}
