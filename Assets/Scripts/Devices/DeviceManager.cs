﻿using Observer;
using Players;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using UnityEngine;

namespace Devices
{
    public class DeviceManager : ISubject, IXmlSerializable
    {
        public IList<Device> _devices;
        private IList<IObserver> _observers;

        private static DeviceManager _deviceManager;

        private DeviceManager()
        {
            _devices = new List<Device>();
            _observers = new List<IObserver>();
        }

        public void AddDevice(Device newDevice)
        {
            Device DevicePreviousSave = GetDevice(newDevice.Name);

            if (DevicePreviousSave == null)
            {
                _devices.Add(newDevice);
            }
            else
            {
                _devices.Remove(DevicePreviousSave);
                _devices.Add(newDevice);
            }

            NotifyObservers(_deviceManager);
        }

        public Device GetDevice(string DeviceName)
        {
            foreach (Device device in _devices)
                if (DeviceName == device.Name)
                    return device;
            return null;
        }

        public int GetDeviceIndex(string deviceName)
        {
            foreach (Device device in _devices)
                if (deviceName == device.Name)
                    return _devices.IndexOf(device);
            return -1;
        }

        public string[] GetAllDevicesAsString()
        {
            string[] devicesNames = new string[_devices.Count];
            int i = 0;
            foreach (Device device in _devices)
            {
                devicesNames[i] = device.Name;
                i++;
            }
            return devicesNames;
        }

        public void DeleteDevice(string DeviceName)
        {
            foreach (Device device in _devices)
                if (DeviceName == device.Name)
                {
                    _devices.Remove(device);
                    NotifyObservers(_deviceManager);
                    return;

                }

        }

        public void SaveDevicesConfigurationToXml(string path, FileMode fileMode)
        {
            var serializer = new XmlSerializer(typeof(DeviceManager), new Type[] { typeof(Device) });
            FileStream fs = new FileStream(path, fileMode);

            serializer.Serialize(fs, this);
            fs.Close();
            Debug.Log("DeviceName Serialization completed");

        }
        public void LoadDevicesConfigurationFromXml(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open);

            XmlSerializer serializer = new XmlSerializer(typeof(DeviceManager), new Type[] { typeof(Device) });

            _devices = ((DeviceManager)serializer.Deserialize(fs))._devices;
            //this = (SignalManager)serializer.Deserialize(fs);

            fs.Close();

            NotifyObservers(PlayerManager.GetInstance());

        }
        public Device GetLastDeviceAdded()
        {
            return _devices[_devices.Count - 1];
        }

        #region Singleton

        public static DeviceManager GetInstance()
        {
            if (_deviceManager == null)
                _deviceManager = new DeviceManager();
            return _deviceManager;
        }

        #endregion

        #region Observer

        public void SubscribeObserver(IObserver observerToSubscribe)
        {
            _observers.Add(observerToSubscribe);
        }

        public void UnsubscribeObserver(IObserver observerToUnsubscribe)
        {
            _observers.Remove(observerToUnsubscribe);
        }

        public void NotifyObservers(object updatedData)
        {
            foreach (IObserver observer in _observers)
            {
                observer.Notify(updatedData);
            }
        }

        #endregion

        #region XmlSerializable

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            _devices.Clear();
            try
            {

                //let's go through all the xml file and break when we arrive at DeviceManager closing tag.
                reader.Read();
                while (true)
                {
                    Debug.Log("@" + reader.Name);
                    if (reader.Name == "DeviceManager")
                        break;

                    Device dev = DeserializeDevice(reader);
                    _devices.Add(dev);
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var device in _devices)
            {
                XmlSerializer serializer = new XmlSerializer(device.GetType());

                serializer.Serialize(writer, device);
                serializer = null;
            }
        }

        private Device DeserializeDevice(XmlReader reader)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Device));
            Device deserializedSignal = (Device)serializer.Deserialize(reader);
            return deserializedSignal;
        }
        #endregion
    }

}