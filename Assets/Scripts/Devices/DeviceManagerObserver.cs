﻿using System.Linq;
using Observer;
using UnityEngine;
using UnityEngine.UI;

namespace Devices
{
    public class DeviceManagerObserver : MonoBehaviour, IObserver
    {

        public Dropdown DropdownList;


        public void Awake()
        {
            DeviceManager.GetInstance().SubscribeObserver(this);
        }

        public void Notify(object updatedData)
        {
            string[] devicesToAdd = DeviceManager.GetInstance().GetAllDevicesAsString();

            DropdownList.ClearOptions();
            Dropdown.OptionData defaultOption = new Dropdown.OptionData
            {
                text = "Select Device"
            };

            if (devicesToAdd == null)
                return;

            DropdownList.options.Add(defaultOption);
            DropdownList.AddOptions(devicesToAdd.ToList());

        }
    } 
}
