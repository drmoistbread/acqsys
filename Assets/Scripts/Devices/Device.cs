﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Reader;
using UnityEngine;

namespace Devices
{
    public class Device : ScriptableObject, IXmlSerializable
    {
        public string Name { set; get; }

        public GameObject DeviceImplementation;

        public string ChannelConnector { get; set; }

        public IReader DeviceReader;

        public static Device CreateInstance(string name, IReader deviceReader, string channelConnector)
        {
            Device newDeviceInstance = CreateInstance<Device>();
            newDeviceInstance.Initialize(name,deviceReader,channelConnector);
            return newDeviceInstance;
        }

        private void Initialize(string name, IReader deviceReader, string channelConnector)
        {
            Name = name;
            DeviceReader = deviceReader;
            ChannelConnector = channelConnector;
        }

        public void SetDeviceGameObject(GameObject device)
        {
            DeviceImplementation = device;
        }

        #region XMLSerialization

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            reader.Read();
            reader.Read();
            Name = reader.Value;
            reader.Read();
            reader.Read();
            reader.Read();
            ChannelConnector = reader.Value;
            reader.Read();
            reader.Read();
            reader.Read();
            string prefabName = reader.Value;
            reader.Read();
            reader.Read();



            Debug.Log("$@" + reader.Name);

            switch (reader.Name)
            {
                case "BitalinoReaderAdapter":
                    DeviceImplementation = (GameObject)Instantiate(Resources.Load("Prefabs/Devices/BITalino", typeof(GameObject)));
                    DeviceImplementation.name = Name;

                    DeviceReader = DeserializeReader(typeof(BitalinoReaderAdapter), reader);
                    DeviceReader.DefineReader(DeviceImplementation.name);
                    break;
                case "BitalinoCPAdapter":
                    DeviceImplementation = (GameObject)Instantiate(Resources.Load("Prefabs/Devices/Mouse-CP", typeof(GameObject)));
                    DeviceImplementation.name = Name;

                    DeviceReader = DeserializeReader(typeof(BitalinoCPAdapter), reader);
                    DeviceReader.DefineReader(DeviceImplementation.name);

                    break;
            }

        }

        public void WriteXml(XmlWriter writer)
        {

            writer.WriteElementString("Name", Name);
            writer.WriteElementString("ChannelConnector", ChannelConnector);
            writer.WriteElementString("GameObject", DeviceImplementation.name);

            string deviceType = DeviceReader.GetType().FullName;
            XmlSerializer serial = new XmlSerializer(Type.GetType(deviceType));
            deviceType = DeviceReader.GetType().FullName;
            serial.Serialize(writer, DeviceReader);
        }

        private IReader DeserializeReader(Type typeOfSignal, XmlReader reader)
        {
            XmlSerializer serializer = new XmlSerializer(typeOfSignal);
            IReader deserializedSignal = (IReader)serializer.Deserialize(reader);
            return deserializedSignal;
        }

        #endregion
    } 
}