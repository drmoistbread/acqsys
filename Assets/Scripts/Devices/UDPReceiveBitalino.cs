﻿using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine.UI;

namespace Devices
{

    public class UDPReceiveBitalino : MonoBehaviour
    {
        private Thread _receiveThread;
        private UdpClient _client;

        private int _port;
        private string PortField;

        public static bool IsConnected;
        public int BufferSize = 2000;
        public string[] frameBuffer;



        //Communication with videogame
        public static bool IsGameSending = false;


        private void Start()
        {
            GameObject parent = transform.gameObject;
            Device dev = DeviceManager.GetInstance().GetDevice(parent.name);
            PortField = dev.ChannelConnector;

            frameBuffer = new string[BufferSize];

            Init();
        }


        public void Init()
        {

            _receiveThread = new Thread(ReceiveData) { IsBackground = true };
            _receiveThread.Start();
            _receiveThread.IsBackground = true;
            IsConnected = true;


        }

        public void ReceiveData()
        {
            _port = int.Parse(PortField);

            _client = new UdpClient(_port);
            while (IsConnected)
            {


                for (int i = 0; i < BufferSize; i++)
                {
                    string compiledData = "";
                    for (int j = 0; j < 4; j++)
                    {
                        try
                        {
                            var ip = new IPEndPoint(IPAddress.Loopback, 0);

                            byte[] udpdata = _client.Receive(ref ip);

                            //  UTF8 encoding in the text format.
                            string data = Encoding.UTF8.GetString(udpdata);
                            // Debug.Log(data);

                            data = TranslateData(data);

                            if (data == "")
                                j--;
                            else
                                compiledData += data;


                        }
                        catch (Exception err)
                        {
                            print(err.ToString());
                        }
                    }
                    frameBuffer[i] = compiledData;
                    //Debug.Log(frameBuffer[i]);
                }
            }

        }

        public string[] getBuffer()
        {
            return frameBuffer;
        }

        //example:
        // data = [$]Analog,[$$]BITalino,[$$$]A4,value,0.0;
        //return "A4,0.0;"
        private string TranslateData(string data)
        {



            string[] separators = { "[$]", "[$$]", "[$$$]", ",", ";", " " };

            var words = data.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            if (words[0] == "Analog" && words[1] == "BITalino" && words[2].StartsWith("A"))
                return words[2] + "," + words[4] + ";";

            return "";



        }

        private void OnApplicationQuit()
        {
            if (IsConnected)
            {
                IsConnected = false;
                _receiveThread.Abort();
                _client.Close();
            }
        }
    }

}