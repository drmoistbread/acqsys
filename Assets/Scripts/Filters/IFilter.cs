﻿namespace Filter
{
    public interface IFilter
    {

        double ApplyFilter(double value);
        void SetFilterValue(string filter,float value);
        IFilter NextFilter { set; get; }
  

    }
}

