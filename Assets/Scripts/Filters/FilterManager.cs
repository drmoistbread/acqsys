﻿namespace Filter
{
    public class FilterManager
    {
        private IFilter _fisrtFilter;
        private readonly DefaultFilter _defaultFilter;

        public FilterManager()
        {
            _defaultFilter = (DefaultFilter)FilterFactory.CreateNewFilter("default");
            _fisrtFilter = _defaultFilter;
        }
        public double ApplyFilter(double value)
        {
            return _fisrtFilter.ApplyFilter(value);
        }

        public void SetFilterValue(string filter, float value)
        {
            _fisrtFilter.SetFilterValue(filter, value);
        }

        public void AddNewFilter(string newFilterString)
        {
            IFilter newFilter = FilterFactory.CreateNewFilter(newFilterString);
            if (_fisrtFilter.GetType() == typeof(DefaultFilter))
            {
                _fisrtFilter = newFilter;
                _defaultFilter.PreviousFilter = newFilter;
                newFilter.NextFilter = _defaultFilter;
                return;
            }

            IFilter filter = _fisrtFilter;

            while (filter.GetType() != typeof(DefaultFilter))
            {
                if (filter.GetType() == newFilter.GetType())
                    return;
                filter = filter.NextFilter;
            }

            if (filter.GetType() == typeof(DefaultFilter))
            {
                IFilter aux = _defaultFilter.PreviousFilter;

                aux.NextFilter = newFilter;
                newFilter.NextFilter = _defaultFilter;
                _defaultFilter.PreviousFilter = newFilter;
            }
        }

        public void UnsetFilter(string filterToUnset)
        {
            IFilter previousFilter = _fisrtFilter;
            IFilter currentFilter = _fisrtFilter;

            while (currentFilter.GetType() != typeof(DefaultFilter))
            {
                if (currentFilter.GetType().ToString().ToLower() == "filter."+filterToUnset)
                {
                    previousFilter.NextFilter = currentFilter.NextFilter;
                    if (_fisrtFilter.GetType() == currentFilter.GetType())
                    {
                        _fisrtFilter = currentFilter.NextFilter;
                    }
                    currentFilter = null; 
                    return;
                }
                previousFilter = currentFilter;
                currentFilter.NextFilter = currentFilter;
            }
        }

    }
}
