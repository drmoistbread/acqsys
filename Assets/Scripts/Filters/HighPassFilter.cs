﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Filter
{

    class HighPassFilter : IFilter
    {
        public IFilter NextFilter { get; set; }

        private float _highpassCutoffFrequency;

        public double ApplyFilter(double value)
        {
            //i get new value then I call the next decorator
            double RC_HP = 1 / (_highpassCutoffFrequency * 2 * Mathf.PI);
            double Alpha_HP = Time.deltaTime / (RC_HP + Time.deltaTime);
            //double _signalHP = _signalHP + Alpha_HP * (value - _signalHP);
            double _signalHP = Alpha_HP * value;

            return NextFilter.ApplyFilter(_signalHP);

        }

        public void SetFilterValue(string filter, float value)
        {
            if (filter.ToLower() == "highpassfilter")
                _highpassCutoffFrequency = value;
            else
                NextFilter.SetFilterValue(filter, value);
        }

      

    }

}
