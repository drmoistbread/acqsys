﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Filter
{

    class LowPassFilter : IFilter
    {

        [Range(20f, 100f)]
        private float _lowpassCutoffFrequency;
        public IFilter NextFilter { get; set; }

        public double ApplyFilter(double value)
        {
            //i get new value then I call the next decorator
            double RC_LP = 1 / (_lowpassCutoffFrequency * 2 * Mathf.PI);
            double Alpha_LP = Time.deltaTime / (RC_LP + Time.deltaTime);
            // double _signalLP = _signalLP + Alpha_LP * (_signalHP - _signalLP);
            double _signalLP = Alpha_LP * (value);

            return NextFilter.ApplyFilter(_signalLP);

        }
        public void SetFilterValue(string filter,float value)
        {

            if (filter.ToLower() == "lowpassfilter")
                _lowpassCutoffFrequency = value;
            else
                NextFilter.SetFilterValue(filter, value);
        }

    }

}