﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Filter
{   class FilterFactory : MonoBehaviour
    {
        public static IFilter CreateNewFilter(string filter)
        {
            switch (filter.ToLower())
            {
                case "lowpassfilter":
                    return new LowPassFilter();
                case "highpassfilter":
                    return new HighPassFilter();
                case "defaultfilter":
                    return new DefaultFilter();
                default:
                    return new DefaultFilter();
            }
        } 
    }
}