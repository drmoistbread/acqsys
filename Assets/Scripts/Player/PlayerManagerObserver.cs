﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Observer;
using UnityEngine;
using UnityEngine.UI;

namespace Players
{

    public class PlayerManagerObserver : MonoBehaviour, IObserver
    {

        public Dropdown DropdownList;


        public void Awake()
        {
            PlayerManager.GetInstance().SubscribeObserver(this);
        }

        public void Notify(object updatedData)
        {
            PlayerManager playersToAdd = (PlayerManager)updatedData;

            DropdownList.ClearOptions();
            Dropdown.OptionData defaultOption = new Dropdown.OptionData();
            defaultOption.text = "Select Player";
            DropdownList.options.Add(defaultOption);
            DropdownList.AddOptions(playersToAdd.GetAllPlayersAsString().ToList());

        }
    } 
}
