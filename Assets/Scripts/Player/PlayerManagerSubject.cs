﻿using System;
using System.Collections;
using System.Collections.Generic;
using Observer;
using UnityEngine;
using UnityEngine.UI;

namespace Players
{

    public class PlayerManagerSubject : MonoBehaviour, ISubject
    {

        public Dropdown DropdownList;

        private IList _observers;

        public PlayerManagerSubject()
        {
            _observers = new List<IObserver>();
        }

        public void Start()
        {

        }
        public void SubscribeObserver(IObserver observerToSubscribe)
        {
            _observers.Add(observerToSubscribe);
        }

        public void UnsubscribeObserver(IObserver observerToUnsubscribe)
        {
            _observers.Remove(observerToUnsubscribe);
        }

        public void NotifyObservers(object updatedData)
        {
            foreach (IObserver observer in _observers)
            {
                observer.Notify(updatedData);
            }
        }

        public void NotifyObservers()
        {
            string playerName = DropdownList.captionText.text;

            NotifyObservers(playerName);
        }
    }

}