﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using Signal;
using UnityEngine;

namespace Players
{
    public class Player
    {

        public string Name { get; set; }
        public Color Colour { get; set; }

        public readonly SignalManager Signals;

        public Player()
        {
            Name = "";
            Signals = new SignalManager();
        }
        public Player(string name)
        {
            Name = name;
            Signals = new SignalManager();
        }

        public void SetColour(Color colour)
        {
            Colour = colour;
        }
    } 
}
