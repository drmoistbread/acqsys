﻿using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Boo.Lang;
using Devices;
using Observer;
using Signal;
using UnityEngine;

namespace Players
{
    public class PlayerManager : ISubject, IXmlSerializable
    {

        #region Declarations

        private static PlayerManager _playerManager;

        private int _lastPlayerAdded;

        public IList _players;

        private readonly IList _observers;

        private readonly Color[] _colours =
    {
        Color.red,
        Color.blue,
        Color.green,
        Color.magenta,
        Color.white,
        Color.yellow,
        Color.clear,
        Color.cyan
    };

        #endregion

        private PlayerManager()
        {
            _players = new List<Player>();
            _observers = new List<IObserver>();
            _lastPlayerAdded = 0;
        }

        public void AddPlayer(Player newPlayer)
        {
            Player playerPreviousSave = GetPlayer(newPlayer.Name);

            if (playerPreviousSave == null)
            {
                if (newPlayer.Name == "")
                {
                    newPlayer.Name = "pl" + (_lastPlayerAdded + 1);
                }
                newPlayer.Colour = _colours[_lastPlayerAdded % _colours.Length];
                _players.Add(newPlayer);
                _lastPlayerAdded++;

            }
            else
            {
                _players.Remove(playerPreviousSave);
                newPlayer.Colour = _colours[_lastPlayerAdded % _colours.Length];
                _players.Add(newPlayer);
            }
        }

        public Player GetPlayer(string playerName)
        {
            foreach (Player player in _players)
                if (playerName == player.Name)
                    return player;
            return null;
        }

        public int GetPlayerIndex(string playerName)
        {
            foreach (Player player in _players)
                if (playerName == player.Name)
                    return _players.IndexOf(player);
            return -1;
        }

        public Player GetPlayerByIndex(int playerIndex)
        {
            return (Player)_players[playerIndex];
        }

        public string[] GetAllPlayersAsString()
        {
            string[] playerNames = new string[_players.Count];
            int i = 0;
            foreach (Player player in _players)
            {
                playerNames[i] = player.Name;
                i++;
            }

            return playerNames;
        }

        public List<Player> GetAllPlayers()
        {
            return (List<Player>)_players;
        }

        public void DeletePlayer(string playerName)
        {
            foreach (Player player in _players)
                if (playerName == player.Name)
                {
                    _players.Remove(player);
                    _lastPlayerAdded--;
                    return;

                }

        }

        public void SaveChannelsConfigurationToXml(string path, FileMode fileMode)
        {
            var serializer = new XmlSerializer(typeof(PlayerManager), new Type[] { typeof(Player) });
            FileStream fs = new FileStream(path, fileMode);

            serializer.Serialize(fs, this);
            fs.Close();
            Debug.Log("Player Serialization completed");

        }

        public void LoadChannelsConfigurationFromXml(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open);

            XmlSerializer serializer = new XmlSerializer(typeof(PlayerManager), new Type[] { typeof(Player) });

            _players = ((PlayerManager)serializer.Deserialize(fs))._players;
            //this = (SignalManager)serializer.Deserialize(fs);

            fs.Close();

            NotifyObservers(PlayerManager.GetInstance());

        }

        #region Singleton

        public static PlayerManager GetInstance()
        {
            if (_playerManager == null)
                _playerManager = new PlayerManager();
            return _playerManager;
        }

        #endregion

        #region Observer

        public void SubscribeObserver(IObserver observerToSubscribe)
        {
            _observers.Add(observerToSubscribe);
        }

        public void UnsubscribeObserver(IObserver observerToUnsubscribe)
        {
            _observers.Remove(observerToUnsubscribe);
        }
        public void UnsubscribeAllObservers()
        {
            _observers.Clear();
        }

        public void NotifyObservers(object updatedData)
        {
            foreach (IObserver observer in _observers)
            {
                observer.Notify(updatedData);
            }
        }

        #endregion

        #region XmlSerializable
        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {


            //let's go through all the xml file and break when we arrive at Signals closing tag.
            while (reader.Read())
            {
                Debug.Log("€" + reader.Name);
                if (reader.Name.ToLower() == "PlayerManager>")
                    break;
                Player player = DeserializePlayer(reader);
                AddPlayer(player);

            }
            foreach (Player player in _players)
            {
                foreach (ISignal signal in player.Signals.Signals)
                {
                    signal.Reader = DeviceManager.GetInstance().GetDevice(signal.DeviceName).DeviceReader;
                }
            }
            NotifyObservers(_playerManager);


        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var player in _players)
            {
                XmlSerializer serializer = new XmlSerializer(player.GetType());

                serializer.Serialize(writer, player);
                serializer = null;
            }
        }

        private Player DeserializePlayer(XmlReader reader)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Player));
            Player deserializedSignal = (Player)serializer.Deserialize(reader);
            return deserializedSignal;
        }

        #endregion

    }

}