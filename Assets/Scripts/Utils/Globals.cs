﻿using System;
using System.Linq;


static class Globals
{

    public const float CHANNEL_BUTTON_Y = -79f,
        CHANNEL_BUTTON_X = -630f,
        GRAPH_INITIAL_X = -450f,
        GRAPH_INITIAL_Y = 0f;
    public static string GetPlayerFromChannelName(string channelName)
    {
        string[] signalName = channelName.Split('-');
        if (signalName != null && signalName.Length > 0)
        {
            return signalName[0];
        }
        return channelName;
    }
    public static string GetSignalFromChannelName(string channelName)
    {
        return channelName;
    }

    public static string RemoveSpaces(string stringWithSpaces)
    {
        return new string(stringWithSpaces.ToCharArray()
            .Where(c => !Char.IsWhiteSpace(c))
            .ToArray());
    }


}


