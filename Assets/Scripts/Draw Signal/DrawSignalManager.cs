﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters;
using Signal;

namespace DrawSignals
{

    public class DrawSignalManager
    {
        #region Declarations

        private const string GRAPH_TEXT = "GraphLineSignal";


        #endregion

        private DrawSignalManager()
        {
        }

        public void CreateNewGraphLine(Color lineColor, ISignal signal, float positionX, float positionY)
        {
            GameObject graphLine = new GameObject(GRAPH_TEXT + signal.Name);
            graphLine.AddComponent(typeof(DrawSignal));

            graphLine.tag = GRAPH_TEXT;

            ConfigureGraphLine(graphLine, lineColor, signal, positionX, positionY);
            UpdateGraphYScale(signal, signal.ScaleY);
        }

        public void UpdateGraphLine(Color lineColor, ISignal signal, float positionX, float positionY)
        {
            GameObject graphLine = GameObject.Find(GRAPH_TEXT + signal.Name);
            if (graphLine == null)
                return;
            ConfigureGraphLine(graphLine, lineColor, signal, positionX, positionY);
        }

        public void UpdateGraphXPosition(ISignal signal, float positionX)
        {
            GameObject graphLine = GameObject.Find(GRAPH_TEXT + signal.Name);
            if (graphLine == null)
                return;
            DrawSignal drawSignal = (DrawSignal)graphLine.GetComponent(typeof(DrawSignal));

            drawSignal.PositionX += positionX;

        }

        public void UpdateLineThickness(ISignal signal, float lineThickness)
        {
            GameObject graphLine = GameObject.Find(GRAPH_TEXT + signal.Name);
            if (graphLine == null)
                return;
            DrawSignal drawSignal = (DrawSignal)graphLine.GetComponent(typeof(DrawSignal));

            drawSignal.SetLineThickness(lineThickness);

        }

        public void UpdateGraphYPosition(ISignal signal, float positionY)
        {
            GameObject graphLine = GameObject.Find(GRAPH_TEXT + signal.Name);

            if (graphLine == null)
                return;
            DrawSignal drawSignal = (DrawSignal)graphLine.GetComponent(typeof(DrawSignal));

            drawSignal.PositionY += positionY;


        }

        public void UpdateGraphYScale(ISignal signal, float scaleY)
        {
            GameObject graphLine = GameObject.Find(GRAPH_TEXT + signal.Name);

            if (graphLine == null)
                return;
            DrawSignal drawSignal = (DrawSignal)graphLine.GetComponent(typeof(DrawSignal));

            drawSignal.ScaleY *= scaleY;
            signal.ScaleY = drawSignal.ScaleY;

        }

        public void DestroyGraphLine(ISignal signal)
        {
            GameObject graphLine = GameObject.Find(GRAPH_TEXT + signal.Name);

            if (graphLine == null)
                return;
            Object.Destroy(graphLine);

        }

        public void DestroyAllGraphLines()
        {
            GameObject[] graphLines = GameObject.FindGameObjectsWithTag(GRAPH_TEXT);

            foreach (var gl in graphLines)
            {
                Object.Destroy(gl);
            }
        }

        public void ConfigureGraphLine(GameObject graphLine, Color lineColor, ISignal signal, float positionX, float positionY)
        {

            DrawSignal drawSignal = (DrawSignal)graphLine.GetComponent(typeof(DrawSignal));

            drawSignal.SetSignal(signal);
            drawSignal.SetColour(lineColor);
            drawSignal.PositionX = positionX;
            drawSignal.PositionY = positionY;

            drawSignal.StartDrawing();

        }



        #region Singleton

        private static DrawSignalManager _drawSignal;

        public static DrawSignalManager GetInstance()
        {
            if (_drawSignal == null)
                _drawSignal = new DrawSignalManager();
            return _drawSignal;
        }

        #endregion

    }

}