﻿using UnityEngine;
using System.Collections;
using Signal;

namespace DrawSignals
{

    public class DrawSignal : MonoBehaviour
    {

        #region Declarations

        private float _lineThickness;

        private LineRenderer _line;

        private ISignal _signal;

        private bool _isDrawable = false;

        public float PositionX { get; set; }

        public float PositionY { get; set; }
        public float ScaleX { get; set; }

        public float ScaleY { get; set; }


        #endregion

        void Awake()
        {
            _lineThickness = 2f;
            _line = gameObject.AddComponent<LineRenderer>();
            _line.material = new Material(Shader.Find("Particles/Additive"));
            _line.startWidth = _lineThickness;
            ScaleY = 1;

        }

        void Update()
        {

            if (!_isDrawable)
                return;

            int i = 0;

            foreach (double signalData in _signal.GetBuffer())
            {
                float posX = (float)(1100f / _signal.Reader.BufferSize * i + PositionX);
                float posY = (float)signalData * ScaleY + PositionY;

                if (posY < -247)
                    posY = -247;
                else if (posY > 316)
                    posY = 316;



                _line.SetPosition(i, new Vector3(posX, posY, 0));

                i++;
            }
        }

        public void SetColour(Color color)
        {

            _line.startColor = color;
            _line.endColor = color;


        }

        public void SetSignal(ISignal signal)
        {
            _signal = signal;
            _line.numPositions = signal.Reader.BufferSize;
        }

        public void SetLineThickness(float lineThickness)
        {
            _line.startWidth = lineThickness;
        }

        public void StartDrawing()
        {
            _isDrawable = true;
        }

        public void StopDrawing()
        {
            _isDrawable = false;
        }




    }

}